package Estructuras;


public class Arco implements Comparable< Arco>  
{

	double peso;
	Vertice salida;
	Vertice llegada;
	char orden;

	public Arco( Vertice psalida , Vertice pllegada , double ppeso , char porden){
		salida=psalida;
		llegada=pllegada;
		peso=ppeso;
		orden =porden;
	}
	
	public double getPeso() {
		return peso;
	}
	public void setPeso(double peso) {
		this.peso = peso;
	}
	public Vertice getSalida() {
		return salida;
	}
	public void setSalida(Vertice salida) {
		this.salida = salida;
	}
	public Vertice getLlegada() {
		return llegada;
	}
	public void setLlegada(Vertice llegada) {
		this.llegada = llegada;
	}

	@Override
	public int compareTo(Arco pcompa) {
		if(peso==pcompa.getPeso()){
			return 0;
		}else if(peso>pcompa.getPeso()){
			return 1;
		}
		return -1;
	}
	
	public int from()
	{
		return this.llegada.getNumero();
	}
	
	public int to()
	{
		return this.salida.getNumero();
	}
	
}
