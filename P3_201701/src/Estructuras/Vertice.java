package Estructuras;

public class Vertice<Key extends Comparable< Key > , Value> 
{
	
	private ListaEncadenada<Arco> arcos;
	private Value valor;
	private Key llave;
	private boolean mack ;
	private int numero;
	
	public Vertice(Value pvalor , Key pllave){
		
		valor=pvalor;
		llave=pllave;
		arcos= new ListaEncadenada<>();
		mack = false;
		
	}
	
	public void visist(){
		mack=!mack;
	}
	
	public boolean getMark(){
		return mack;
	}
	
	public void anadirArco( Arco parco){
		arcos.agregarElementoFinal(parco);
	}
	
	public void organizarArcos(){
		arcos.darElementoPosicionActual();
	}
	
	public int getNumArcos(){
		return arcos.darNumeroElementos();
	}

	public ListaEncadenada<Arco> getArcos() {
		return arcos;
	}

	public void setArcos(ListaEncadenada<Arco> arcos) {
		this.arcos = arcos;
	}

	public Value getValor() {
		return valor;
	}

	public void setValor(Value valor) {
		this.valor = valor;
	}

	public Key getLlave() {
		return llave;
	}

	public void setLlave(Key llave) {
		this.llave = llave;
	}
	
	public void setNumero(int n)
	{
		numero = n;
	}
	
	public int getNumero()
	{
		return numero;
	}
}

