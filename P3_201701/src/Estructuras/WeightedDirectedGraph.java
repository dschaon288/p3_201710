package Estructuras;

import java.util.Iterator;

public class WeightedDirectedGraph <Key extends Comparable<Key> ,Value>{

	ListaEncadenada<Vertice<Key, Value>> vertices;
	int count=0;
	int cantidad = 0;

	NodoCamino<Key> camino ;

	public WeightedDirectedGraph(){
		vertices= new ListaEncadenada<Vertice<Key, Value>>();
	}

	public int numVertices(){
		return vertices.darNumeroElementos();
	}

	public Vertice<Key, Value> darVertice(Key id){

		Iterator<Vertice<Key, Value>> iter = vertices.iterator();
		boolean encontro=false;
		Vertice<Key, Value> tvertice = null ;
		while(iter.hasNext()&&!encontro) {
			tvertice=iter.next();
			if( tvertice.getLlave().equals(id)){
				encontro=true;
			}
		}
		return tvertice;
	}

	public void agregarVertice(Key id , Value infover){
		Vertice<Key, Value> v = new Vertice<Key, Value>(infover, id);
		vertices.agregarElementoFinal(v);
		cantidad++;
		v.setNumero(cantidad);
	}

	public int numeroArcos(){
		int nArcos=0;
		Iterator<Vertice<Key, Value>> iter = vertices.iterator();
		while(iter.hasNext()) {
			nArcos+=iter.next().getNumArcos();
		}
		return nArcos;
	}

	public double darPesoArco(Key idOrigen, Key idDestino){

		Iterator<Vertice<Key, Value>> iter = vertices.iterator();
		Vertice<Key, Value> actual= null;
		double peso=-1;
		boolean encontro=false;
		while(iter.hasNext()&&!encontro) {
			actual=iter.next();
			Iterator<Arco> iterArco = actual.getArcos().iterator();
			while(iterArco.hasNext()&&!encontro){
				Arco arco = iterArco.next();
				if(arco.salida.getLlave().equals(idOrigen) && arco.llegada.getLlave().equals(idDestino)){
					peso=arco.peso;
					encontro=true;
				}
			}
		}
		return peso;
	}


	public void agregarArco(Key idOrigen, Key idDestino, double peso){
		Iterator<Vertice<Key, Value>> iter = vertices.iterator();
		Vertice<Key, Value> actual = null;
		boolean agrego=false;
		Vertice<Key, Value> llegada;

		while(iter.hasNext()&&!agrego) {
			actual=iter.next();
			if(actual.getLlave().equals(idOrigen)){
				llegada= darVertice(idDestino);
				actual.anadirArco(new Arco(actual,llegada,peso,'\0'));
				agrego=true;
			}
		}
	}

	public void agregarArco(Key idOrigen, Key idDestino, double peso, char ordenLexicografico){
		Iterator<Vertice<Key, Value>> iter = vertices.iterator();
		Vertice<Key, Value> actual = null;
		boolean agrego=false;
		Vertice<Key, Value> llegada;

		while(iter.hasNext()&&!agrego) {
			actual=iter.next();
			if(actual.getLlave().equals(idOrigen)){
				llegada= darVertice(idDestino);
				actual.anadirArco(new Arco(actual,llegada,peso, ordenLexicografico));
				agrego=true;
			}
		}
	}

	public Iterator<Vertice<Key, Value>> darVertices(){
		return vertices.iterator();
	}
	public int darGrado(Key id){

		Iterator<Vertice<Key, Value>> iter = vertices.iterator();
		Vertice<Key, Value> actual = null;
		boolean encontro=false;
		int grado =-1;
		while(iter.hasNext()&&!encontro) {
			actual=iter.next();
			if(actual.getLlave().equals(id)){
				grado=actual.getArcos().darNumeroElementos();
				encontro=true;
			}
		}
		return grado;
	}

	public Iterator<Key > darVerticesAdyacentes(Key id){
		Iterator<Vertice<Key, Value>> iter = vertices.iterator();
		Vertice<Key, Value> actual = null;
		boolean encontro=false;
		Iterator<Key> grado = null;
		while(iter.hasNext()&&!encontro) {
			actual=iter.next();
			if(actual.getLlave().equals(id)){
				Iterator<Arco> iterArco = actual.getArcos().iterator();
				Arco tarco = iterArco.next();
				ListaEncadenada<Key> local = new ListaEncadenada<>();
				while(iterArco.hasNext()){
					tarco=iterArco.next();
					local.agregarElementoFinal((Key) (tarco.getLlegada()).getLlave());
				}
				grado=local.iterator();
				encontro=true;
			}
		}
		return grado;
	}

	public void desmarcar(){
		Iterator<Vertice<Key, Value>> iter = vertices.iterator();
		Vertice<Key, Value> local;
		while(iter.hasNext()) {
			local = iter.next();
			if(local.getMark()){
				local.visist();
			}
		}
	}

	public NodoCamino<Key> DFS( Key origen ){
		Vertice<Key, Value> local = this.darVertice(origen);
		camino = new NodoCamino<Key>();
		camino.setOrigen(origen);
		dfs( local);
		desmarcar();
		return camino;
	}

	private void dfs( Vertice origen ){

		double costo=0;
		Double larco=Math.random()*origen.getNumArcos();
		origen.visist();
		count++;
		Iterator<Arco> iter = origen.getArcos().iterator();
		Arco actual ;
		while(iter.hasNext()){
			actual=iter.next();
			actual=(Arco) origen.getArcos().darElemento(larco.intValue());
			costo= actual.getPeso(); 
			if(!actual.getLlegada().getMark()){
				camino.agregarVertice((Key) origen.getLlave(),costo );
				dfs(actual.getLlegada());
			}
		}
	}

	public NodoCamino<Key> darCaminoDFS( Key origen , Key destino){

		Vertice<Key, Value> vOrigen = darVertice(origen);
		Vertice<Key, Value> vDestino = darVertice(destino);

		double costo=0;
		boolean encontro = false;
		NodoCamino<Key> rta = new NodoCamino<Key>();
		vOrigen.visist();
		count++;
		Iterator<Arco> iter = vOrigen.getArcos().iterator();
		Arco actual ;
		while(iter.hasNext()){
			actual=iter.next();
			costo= actual.getPeso(); 
			if(!actual.getLlegada().getMark()){
				camino.agregarVertice((Key) vOrigen.getLlave(),costo );
				rta.agregarVertice((Key) vOrigen.getLlave(), costo);
				if(actual.getLlegada() == destino)
				{
					encontro = true;
					break;
				}
				dfs(actual.getLlegada());
			}

		}

		if(!encontro)
		{
			return null;
		}

		return rta;
	}

	public NodoCamino<Key> BFS(Key origen ){
		Vertice<Key, Value> local = this.darVertice(origen);
		camino = new NodoCamino<Key>();
		camino.setOrigen(origen);
		bfs( local);
		desmarcar();
		return camino;
	}

	public void bfs(Vertice origen )
	{
		Queue<Vertice> cola  = new Queue<>();
		double costo = 0;
		origen.visist();
		cola.enqueue(origen);
		while(!cola.isEmpty())
		{
			Vertice<Key, Value> v = cola.dequeue();
			Iterator<Arco> iter = origen.getArcos().iterator();
			Arco actual ;
			while(iter.hasNext()){
				actual=iter.next();
				costo= actual.getPeso(); 
				if(!actual.getLlegada().getMark()){
					camino.agregarVertice((Key) origen.getLlave(),costo );
					actual.getLlegada().visist();
					cola.enqueue(actual.getLlegada());
				}
			}
		}
	}
	public  NodoCamino<Key> darCaminosBFS( Key origen , Key destino){
		
		Vertice<Key, Value> vOrigen = darVertice(origen);
		Vertice<Key, Value> vDestino = darVertice(destino);
		boolean encontro = false;
		NodoCamino<Key> rta = new NodoCamino<>();
		
		Queue<Vertice> cola  = new Queue<>();
		double costo = 0;
		vOrigen.visist();
		cola.enqueue(vOrigen);
		while(!cola.isEmpty())
		{
			Vertice<Key, Value> v = cola.dequeue();
			Iterator<Arco> iter = vOrigen.getArcos().iterator();
			Arco actual ;
			while(iter.hasNext()){
				actual=iter.next();
				costo= actual.getPeso(); 
				if(!actual.getLlegada().getMark()){
					camino.agregarVertice((Key) vOrigen.getLlave(),costo );
					rta.agregarVertice((Key) vOrigen.getLlave(),costo);
					if(actual.getLlegada() == destino)
					{
						encontro = true;
						break;
					}
					actual.getLlegada().visist();
					cola.enqueue(actual.getLlegada());
				}
			}
		}
		
		if(!encontro)
		{
			return null;
		}
		
		return rta;
	}


	public ListaEncadenada<Arco> arcosAdyacentesL(int v)
	{
		ListaEncadenada<Arco> rta = new ListaEncadenada<>();
		if(vertices.darElemento(v) != null)
		{
			rta = vertices.darElemento(v).getArcos();
		}
		
		return rta;
	}
	
	public Iterator<Arco> arcosAdyacentes(int v)
	{
		ListaEncadenada<Arco> rta = new ListaEncadenada<>();
		if(vertices.darElemento(v) != null)
		{
			rta = vertices.darElemento(v).getArcos();
		}
		
		return rta.iterator();
	}
	









}
