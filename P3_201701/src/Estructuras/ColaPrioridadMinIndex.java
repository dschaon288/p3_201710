package Estructuras;


/******************************************************************************
 *  Compilation:  javac IndexMinPQ.java
 *  Execution:    java IndexMinPQ
 *  Dependencies: StdOut.java
 *
 *  Minimum-oriented indexed PQ implementation using a binary heap.
 *
 * Sacado de http://algs4.cs.princeton.edu/24pq/IndexMinPQ.java.html
 ******************************************************************************/

import java.util.Iterator;
import java.util.NoSuchElementException;

public class ColaPrioridadMinIndex<T extends Comparable<T>> implements Iterable<Integer> {

	
	
	private int max;
	private int n;
	private int[] pq;
	private int[] qp;
	private T[] llaves;	

	public ColaPrioridadMinIndex(int max)
	{
		if(max < 0){ throw new IllegalArgumentException();}

		this.max = max;
		n = 0;
		llaves = (T[])new Comparable[max + 1];
		pq = new int[max + 1];
		qp = new int[max + 1];
		for(int i = 0; i <= max; i++)
		{
			qp[i] = -1;
		}
	}

	public boolean isEmpty() {
		return n == 0;
	}

	public boolean contiene(int i) {
		if (i < 0 || i >= max) throw new IndexOutOfBoundsException();
		return qp[i] != -1;
	}

	
	public int tamanio() {
		return n;
	}

	public void insertar(int i, T t) {
		if (i < 0 || i >= max) throw new IndexOutOfBoundsException();
		if (contiene(i)) throw new IllegalArgumentException("index is already in the priority queue");
		n++;
		qp[i] = n;
		pq[n] = i;
		llaves[i] = t;
		swim(n);
	}

	public int minIndex() {
		if (n == 0) throw new NoSuchElementException();
		return pq[1];
	}

	public T minT() {
		if (n == 0) throw new NoSuchElementException();
		return llaves[pq[1]];
	}

	public int delMin() {
		if (n == 0) throw new NoSuchElementException();
		int min = pq[1];
		exch(1, n--);
		sink(1);
		assert min == pq[n+1];
		qp[min] = -1;        // delete
		llaves[min] = null;    // to help with garbage collection
		pq[n+1] = -1;        // not needed
		return min;
	}

	public T llaveDe(int i) {
		if (i < 0 || i >= max) throw new IndexOutOfBoundsException();
		if (!contiene(i)) throw new NoSuchElementException("index is not in the priority queue");
		else return llaves[i];
	}

	public void cambiarT(int i, T t) {
		if (i < 0 || i >= max) throw new IndexOutOfBoundsException();
		if (!contiene(i)) throw new NoSuchElementException("index is not in the priority queue");
		llaves[i] = t;
		swim(qp[i]);
		sink(qp[i]);
	}

	public void cambiar(int i, T t) {
		cambiarT(i, t);
	}

	public void disminuirLlave(int i, T t) {
		if (i < 0 || i >= max) throw new IndexOutOfBoundsException();
		if (!contiene(i)) throw new NoSuchElementException("index is not in the priority queue");
		if (llaves[i].compareTo(t) <= 0)
			throw new IllegalArgumentException("Calling decreaseKey() with given argument would not strictly decrease the key");
		llaves[i] = t;
		swim(qp[i]);
	}

	public void aumentarLlave(int i, T t) {
		if (i < 0 || i >= max) throw new IndexOutOfBoundsException();
		if (!contiene(i)) throw new NoSuchElementException("index is not in the priority queue");
		if (llaves[i].compareTo(t) >= 0)
			throw new IllegalArgumentException("Calling increaseKey() with given argument would not strictly increase the key");
		llaves[i] = t;
		sink(qp[i]);
	}

	public void eliminar(int i) {
		if (i < 0 || i >= max) throw new IndexOutOfBoundsException();
		if (!contiene(i)) throw new NoSuchElementException("index is not in the priority queue");
		int index = qp[i];
		exch(index, n--);
		swim(index);
		sink(index);
		llaves[i] = null;
		qp[i] = -1;
	}

	private boolean mayor(int i, int j) {
		return llaves[pq[i]].compareTo(llaves[pq[j]]) > 0;
	}

	private void exch(int i, int j) {
		int swap = pq[i];
		pq[i] = pq[j];
		pq[j] = swap;
		qp[pq[i]] = i;
		qp[pq[j]] = j;
	}

	private void swim(int k) {
		while (k > 1 && mayor(k/2, k)) {
			exch(k, k/2);
			k = k/2;
		}
	}

	private void sink(int k) {
		while (2*k <= n) {
			int j = 2*k;
			if (j < n && mayor(j, j+1)) j++;
			if (!mayor(k, j)) break;
			exch(k, j);
			k = j;
		}
	}

	public Iterator<Integer> iterator() { return new HeapIterator(); }

	private class HeapIterator implements Iterator<Integer> {
		private ColaPrioridadMinIndex<T> copy;

		public HeapIterator() {
			copy = new ColaPrioridadMinIndex<T>(pq.length - 1);
			for (int i = 1; i <= n; i++)
				copy.insertar(pq[i], llaves[pq[i]]);
		}

		public boolean hasNext()  { return !copy.isEmpty();                     }
		public void remove()      { throw new UnsupportedOperationException();  }

		public Integer next() {
			if (!hasNext()) throw new NoSuchElementException();
			return copy.delMin();
		}
	}
}