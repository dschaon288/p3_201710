package view;

import java.util.Iterator;
import java.util.Random;
import java.util.Scanner;

import API.FestivalTeatro;
import Estructuras.ILista;
import Estructuras.ListaEncadenada;
import model.VOFranquicia;
import model.VOGeneroPelicula;
import model.VOPeliculaPlan;
import model.VOTeatro;
import model.VOUsuario;


public class Vista{
	
	public static void main(String[] args) {

		Scanner sc=new Scanner(System.in);
		FestivalTeatro festival= new FestivalTeatro(); ;
		boolean fin=false;

		while(!fin){
			printMenu();

			int option = sc.nextInt();
			String x= null;
			int id=0;
			int dia=0;
			String genero="";
			ListaEncadenada<VOPeliculaPlan> rta;
			switch(option){
			case 1:
				festival.crearSR();
				festival.cargarpeliculas();
				festival.cargarTeatros("Data/teatros_v3.json");
				festival.cargarCartelera("Data/programacion/dia1.json");
				festival.cargarRed("Data/tiempos.json");
				festival.cargarSimilitudes();
				festival.cargarUsuarios();
				System.out.println(" Cree el Festival \n"
									+ "se cargo la Red \n"
									+ "se cargo  los teatros \n"
									+ "se cargo la cartelera \n"
									+ "se cargo las similitudes \n"
									+ "se cargo los usuarios  \n" );
				break;
			case 2:
				
				System.out.println("Escibra el id del usuario");
				id = sc.nextInt();
				VOUsuario usuario = new VOUsuario();
				usuario.setId(id);
				System.out.println("Escibra la fecha ");
				dia =sc.nextInt();
				rta = festival.PlanPeliculas(usuario,dia);
				Iterator<VOPeliculaPlan> iter = rta.iterator();
				while (iter.hasNext()) {
					VOPeliculaPlan plan = (VOPeliculaPlan) iter.next();
					System.out.println("Teatro: "+plan.getTeatro().getNombre()+"\n"
										+"Pelicula: "+plan.getPelicula().getTitulo()+"\n"
										+"Hora: "+plan.getHoraInicio().getHours()+"\n"	
										+"Dia: "+plan.getDia());
				}
				break;
				
			case 3:
				System.out.println("Escibra el id del usuario");
				id = sc.nextInt();
				VOUsuario usuario2 = new VOUsuario();
				usuario2.setId(id);
				System.out.println("Escibra el genero ");
				genero =sc.next();
				VOGeneroPelicula rt = new VOGeneroPelicula();
				rt.setNombre(genero);
				rta = (ListaEncadenada<VOPeliculaPlan>) festival.PlanPorGenero(rt, usuario2);
				Iterator<VOPeliculaPlan> iter2 = rta.iterator();
				System.out.println("Genero :"+genero);
				while (iter2.hasNext()) {
					VOPeliculaPlan plan = (VOPeliculaPlan) iter2.next();
					System.out.println("Teatro: "+plan.getTeatro().getNombre()+"\n"
										+"Pelicula: "+plan.getPelicula().getTitulo()+"\n"
										+"Hora: "+plan.getHoraInicio().getHours()+"\n"	
										+"Dia: "+plan.getDia()+"\n"
										+"Generos: "+plan.getPelicula().toGeneros());
				}
				break;
				
			case 4:
				System.out.println("Escibra la Franquicia");
				x=sc.next();
				String frt=x+" "+sc.next();
				VOFranquicia franquicia = new VOFranquicia();
				franquicia.setNombre(frt);
				System.out.println("Escriba el dia ");
				dia=sc.nextInt();
				System.out.println("Escibra la franja(Mañana,Tarde,Noche) ,NO en caso que no quiera una franja");
				x=sc.next();
				ILista<VOPeliculaPlan> rta4 = festival.PlanPorFranquicia(franquicia, dia, x.toUpperCase());
				for (int i = 0; i < rta4.darNumeroElementos(); i++) {
					VOPeliculaPlan mostrar=rta4.darElemento(i);
					System.out.println("Teatro: "+mostrar.getTeatro().getNombre()+"\n"
							+"Pelicula: "+mostrar.getPelicula().getTitulo()+"\n"
							+"Hora: "+mostrar.getHoraInicio().getHours()+"\n"	
							+"Dia: "+mostrar.getDia());
				}
				
				break;
				
			case 5:
				System.out.println("Escriba el dia ");
				dia=sc.nextInt();
				System.out.println("Escriba el Genero ");
				x=sc.next();
				VOGeneroPelicula genero2 = new VOGeneroPelicula();
				genero2.setNombre(x);
				ILista<VOPeliculaPlan> rta5 =festival.PlanPorGeneroYDesplazamiento(genero2, dia);
				for (int i = 0; i < rta5.darNumeroElementos(); i++) {
					VOPeliculaPlan mostrar=rta5.darElemento(i);
					System.out.println("Teatro: "+mostrar.getTeatro().getNombre()+"\n"
							+"Pelicula: "+mostrar.getPelicula().getTitulo()+"\n"
							+"Hora: "+mostrar.getHoraInicio().getHours()+"\n"	
							+"Dia: "+mostrar.getDia());
				}
				break;
			case 6:
				System.out.println("Escriba el dia ");
				dia=sc.nextInt();
				System.out.println("Escriba el genero ");
				x=sc.next();
				System.out.println("Escriba la franquicia ");
				String franq =sc.next();
				String fran2=franq+" "+sc.next();
				VOGeneroPelicula genero3 = new VOGeneroPelicula();
				genero3.setNombre(x);
				VOFranquicia franquicia2 = new VOFranquicia();
				franquicia2.setNombre(fran2);
				festival.PlanPorGeneroDesplazamientoYFranquicia(genero3, dia , franquicia2);
				break;
			case 7:

				break;
			case 8:
				System.out.println("Escriba el teatro ");
				String origent  =sc.next();
				System.out.println("Escriba el maximo ");
				dia  =sc.nextInt();
				VOTeatro origen = new VOTeatro("Cine Colombia Avenida Chile", null , null);
				ListaEncadenada<ListaEncadenada<VOTeatro>> rta10 = festival.rutasPosible(origen , dia);
				for(int i = 0; i < rta10.darNumeroElementos(); i++)
				{
					ListaEncadenada<VOTeatro> act = rta10.darElemento(i);
					for(int j = 0; j < act.darNumeroElementos(); j++)
					{
						VOTeatro t = act.darElemento(j);
						System.out.println("Teatro: " + t.getNombre());
					}
					System.out.println("-------------------------------------------------------");
				}
				break;
				
			case 9:
				System.out.println("Adios");
				sc.close();
				return;
			}
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Proyecto 3----------------------");
		System.out.println("1.Cargar (R.1,R.2,R.3)");
		System.out.println("2. Requerimiento 4");
		System.out.println("3. Requerimiento 5");
		System.out.println("4. Requerimiento 6");
		System.out.println("5. Requerimiento 7");
		System.out.println("6. Requerimiento 8");
		System.out.println("7. Requerimiento 9");
		System.out.println("8. Requerimiento 10");
		System.out.println("9.Salir");
		System.out.println("Type the option number for the task, then press enter: (e.g., 1):");

	}


}
