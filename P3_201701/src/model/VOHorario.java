package model;

import java.util.Date;

import Estructuras.RedBlackBST;

public class VOHorario implements Comparable<VOHorario> {
	
	private Date hora;
	private RedBlackBST<Integer,VOPelicula>pelicula=new RedBlackBST<>();
	
	public Date getHora() {
		return hora;
	}
	public void setHora(Date hora) {
		this.hora = hora;
	}
	public RedBlackBST<Integer, VOPelicula> getPelicula() {
		return pelicula;
	}
	public void setPelicula(RedBlackBST<Integer, VOPelicula> pelicula) {
		this.pelicula = pelicula;
	}
	
	public void agregarPelicula(Integer id , VOPelicula nueva){
		pelicula.put(id, nueva);
	}
	@Override
	public int compareTo(VOHorario cmp) {
		if(hora.getHours()>cmp.getHora().getHours()){
			return 1;
		}else if(hora.getHours()<cmp.getHora().getHours()){
			return -1;
		}else{
			return 0;
		}
		
	}
	public boolean estaPeli(VOPelicula option) {
		// TODO Auto-generated method stub
		return pelicula.contains(option.getId());
	}
}
