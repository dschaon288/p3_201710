package model;

import Estructuras.ListaEncadenada;
import Estructuras.RedBlackBST;
import model.VOFranquicia;
import model.VOUbicacion;

public class VOTeatro implements Comparable<VOTeatro> {

	
	/**
	 * Atributo que modela el nombre del teatro
	 */
	
	private String nombre;
	

	/**
	 * Atributo que modela la ubicacion del teatro 
	 */
	
	private VOUbicacion ubicacion;
	
	/**
	 * Atributo que referencia la franquicia
	 */
	
	private VOFranquicia franquicia;
	
	
	private RedBlackBST<Integer,VOHorario> horarios = new RedBlackBST<>() ;
	
	public VOTeatro(String pnombre, VOUbicacion pubicacion , VOFranquicia pfrabquicia ){
		
		nombre=pnombre;
		ubicacion=pubicacion;
		franquicia= pfrabquicia;
		
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public VOUbicacion getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(VOUbicacion ubicacion) {
		this.ubicacion = ubicacion;
	}

	public VOFranquicia getFranquicia() {
		return franquicia;
	}

	public void setFranquicia(VOFranquicia franquicia) {
		this.franquicia = franquicia;
	}
	
	public void setHorarios(RedBlackBST<Integer,VOHorario> ppelis){
		horarios=ppelis;
	}
	public RedBlackBST<Integer,VOHorario> getHorarios (){
		return horarios;
	}

	@Override
	public int compareTo(VOTeatro cmp) {
		if(horarios.size()>cmp.horarios.size()){
			return 1;
		}else if(horarios.size()<cmp.horarios.size()){
			return -1;
		}else{
			return 0;
		}
		
	}

	
	
}
