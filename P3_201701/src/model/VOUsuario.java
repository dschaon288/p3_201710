package model;

import Estructuras.RedBlackBST;

/**
 *  
 * @Author: Tomas F. Venegas 
 */
public class VOUsuario {
	
	/**
	 * Atributo que modela el id del usuario
	 */

	private int id;
	//Arbol de ratings , en donde la llave es el id de la pelicula y el valor es el rating
	private RedBlackBST<Integer,Integer> Ratings= new RedBlackBST<>();
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public RedBlackBST<Integer, Integer> getRatings() {
		return Ratings;
	}

	public void setRatings( Integer key, Integer rating) {
		Ratings.put(key, rating);
	}
	
	
}
