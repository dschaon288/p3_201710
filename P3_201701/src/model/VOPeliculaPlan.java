package model;

import java.util.Date;



/**
 *  
 * @Author: Tomas F. Venegas 
 */
public class VOPeliculaPlan implements Comparable<VOPeliculaPlan>{
	
	/**
	 * Atributo que apunta a la pelicula que se propone
	 */
	private VOPelicula pelicula;
	
	/**
	 * Atributo que apunta al teatro que se propone
	 */
	private VOTeatro teatro;
	
	/**
	 * Atributo que modela la hora de inicio de la pelicula
	 */
	private Date horaInicio;
	
	/**
	 * Atributo que modela la hora de fin de la pelicula
	 */
	private Date horaFin;
	
	/**
	 * Atributo que modela el dia de presentacion de la pelicla
	 * (1..5)
	 */
	
	private int dia;
	
	public VOPeliculaPlan() {
		// TODO Auto-generated constructor stub
	}

	public VOPelicula getPelicula() {
		return pelicula;
	}

	public void setPelicula(VOPelicula pelicula) {
		this.pelicula = pelicula;
	}

	public VOTeatro getTeatro() {
		return teatro;
	}

	public void setTeatro(VOTeatro teatro) {
		this.teatro = teatro;
	}

	public Date getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(Date horaInicio) {
		this.horaInicio = horaInicio;
	}

	public Date getHoraFin() {
		return horaFin;
	}

	public void setHoraFin(Date  horaFin) {
		this.horaFin = horaFin;
	}

	public int getDia() {
		return dia;
	}

	public void setDia(int dia) {
		this.dia = dia;
	}

	@Override
	public int compareTo(VOPeliculaPlan o) {
		if(horaInicio.after(o.horaInicio)){
			return -1;
		}else if(horaInicio.before(o.horaInicio)){
			return 1;
		}else{
			return 0;
		}
	}
	
	
	

}
