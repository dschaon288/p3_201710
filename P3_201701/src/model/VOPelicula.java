package model;

import java.util.Date;

import javax.net.ssl.HostnameVerifier;

import Estructuras.ListaEncadenada;
import Estructuras.RedBlackBST;

public class VOPelicula {
	
	private int id;
	private String titulo;
	private String[] generos;
	
	
	public VOPelicula(int id, String titulo, String[] generos) {
	
		this.id = id;
		this.titulo = titulo;
		this.generos = generos;
		
	}
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String[] getGeneros() {
		return generos;
	}
	public void setGeneros(String[] generos) {
		this.generos = generos;
	}
	
	public boolean esGenero(String genero){
		
		boolean rta= false;
		
		for (int i = 0; i < generos.length&& !rta; i++) {
			if(generos[i].equalsIgnoreCase(genero)){
				rta =true;
			}
		}
		
		return rta;
	}
	
	public String toGeneros(){
			String rta="";
		
		for (int i = 0; i < generos.length; i++) {
				rta +=generos[i]+"//";
		}
		
		return rta;
	}

}
