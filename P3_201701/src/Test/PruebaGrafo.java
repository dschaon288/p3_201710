package Test;

import Estructuras.Vertice;
import Estructuras.WeightedDirectedGraph;
import junit.framework.TestCase;

public class PruebaGrafo extends TestCase {
	
	private  WeightedDirectedGraph<Integer, String > grafo;
	
	private void setupEscenario1()
	{
		grafo = new WeightedDirectedGraph<>();
	}
	
	private void setupEscenario2( ){
		
		setupEscenario1();
		grafo.agregarVertice(1,"1");
		grafo.agregarVertice(2,"2");
		grafo.agregarVertice(3,"3");
		grafo.agregarVertice(4,"4");
		grafo.agregarVertice(5,"5");
		grafo.agregarVertice(6,"6");
		grafo.agregarVertice(7,"7");
		grafo.agregarVertice(8,"8");
		grafo.agregarVertice(9,"9");
		
		grafo.agregarArco(1, 5, 4, 'a');
		
		grafo.agregarArco(5, 7, 5, 'a');
		grafo.agregarArco(5, 2, 8, 'b');
		grafo.agregarArco(5, 4, 8, 'c');
		grafo.agregarArco(5, 6 ,5, 'd');
		
		grafo.agregarArco(7, 9, 6, 'a');
		grafo.agregarArco(7, 2, 1, 'b');
		grafo.agregarArco(7, 1, 10, 'c');
		
		grafo.agregarArco(9, 1, 4, 'a');
		
		grafo.agregarArco(2, 9, 4, 'a');
		grafo.agregarArco(2, 4, 1, 'b');
		
		grafo.agregarArco(4, 8, 3, 'a');
		
		grafo.agregarArco(8, 3, 4, 'a');
		
		grafo.agregarArco(3, 1, 2, 'a');
		grafo.agregarArco(3, 5, 4, 'b');
		
		grafo.agregarArco(6, 4, 4, 'a');
		grafo.agregarArco(6, 8, 6, 'b');
		grafo.agregarArco(6, 3, 6, 'c');
		
	}
	
	public void testDarGrado(){
		setupEscenario2();
		assertEquals(4,grafo.darGrado(5));
		assertEquals(2,grafo.darGrado(2) );
		assertEquals(2,grafo.darGrado(3) );
	}
	
	public void testDarPesoArco(){
		setupEscenario2();
		assertEquals(10, grafo.darPesoArco(7, 1), 0.1);
		assertEquals(4,grafo.darPesoArco(1, 5),0.1);
		assertEquals(4,grafo.darPesoArco(8, 3),0.1);
		assertEquals(3,grafo.darPesoArco(4, 8),0.1);
	}
	
	public void testDarVertice(){
		setupEscenario2();
		Vertice< Integer, String> local;
		local=grafo.darVertice(6);
		assertEquals("6", local.getValor());
		local=grafo.darVertice(2);
		assertEquals("2", local.getValor());
		local=grafo.darVertice(9);
		assertEquals("9", local.getValor());
	}
	
	public void testDesmarcar(){
		setupEscenario2();
		grafo.darVertice(1).visist();
		grafo.darVertice(2).visist();
		grafo.darVertice(3).visist();
		grafo.darVertice(4).visist();
	}
	
	public void testNumeroArcos(){
		setupEscenario2();
		assertEquals(18, grafo.numeroArcos());
	}
	
	public void testVertices(){
		setupEscenario2();
		assertEquals(9, grafo.numVertices());
	}
	
}
