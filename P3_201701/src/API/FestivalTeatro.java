package API;

import java.io.BufferedReader;
import java.io.FileReader;
import java.lang.management.PlatformLoggingMXBean;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Random;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import Estructuras.Arco;
import Estructuras.ColaPrioridad;
import Estructuras.Dijkstra;
import Estructuras.ILista;
import Estructuras.ListaEncadenada;
import Estructuras.NodoCamino;
import Estructuras.RedBlackBST;
import Estructuras.Stack;
import Estructuras.Vertice;
import Estructuras.WeightedDirectedGraph;
import model.VOFranquicia;
import model.VOGeneroPelicula;
import model.VOHorario;
import model.VOPelicula;
import model.VOPeliculaPlan;
import model.VOTeatro;
import model.VOUbicacion;
import model.VOUsuario;

public class FestivalTeatro implements ISistemaRecomendacion{
	
	
	//hORAS DISPONIBLES EN EL FESTIVAL DE TEATRO (HAY PELICULAS DE LAS 8 AM A LAS 10 PM )
	private static final int HORASFESTIVAL=16;
	
	private RedBlackBST<Integer, WeightedDirectedGraph<String,VOTeatro>> grafos;
	private WeightedDirectedGraph<String,VOTeatro> Teatros ;
	private RedBlackBST<Integer,VOPelicula> peliculas;
	private RedBlackBST<Integer,RedBlackBST<Integer,Double>> similitudes;
	private RedBlackBST<Integer, VOUsuario> usuarios ;
	
	@Override
	public ISistemaRecomendacion crearSR() {
		grafos= new RedBlackBST<>();
		Teatros = new WeightedDirectedGraph<>();
		peliculas=new RedBlackBST<>();
		similitudes= new RedBlackBST<>();
		usuarios= new RedBlackBST<>();
		return null;
	}
	@Override
	public boolean cargarTeatros(String ruta) {
		try{
			FileReader fl = new FileReader(ruta);
			JsonParser parser= new JsonParser();
			JsonArray arreglo = (JsonArray) parser.parse(fl);
			Iterator<JsonElement> iter = arreglo.iterator();
			while(iter.hasNext()){
				
				JsonObject actual = (JsonObject) iter.next();
				String nombre = actual.get("Nombre").getAsString();
				VOUbicacion ubicacion=  new VOUbicacion();
				VOFranquicia franquicia= new VOFranquicia();
				franquicia.setNombre(actual.get("Franquicia").getAsString());
				String cordenadas = actual.get("Ubicacion geografica (Lat|Long)").getAsString();
				String lat= cordenadas.split("\\|")[0] ;
				String log= cordenadas.split("\\|")[1]; 
				
				ubicacion.setLatitud(Double.parseDouble(lat));
				ubicacion.setLongitud(Double.parseDouble(log));
				
				VOTeatro nuevo = new VOTeatro(nombre, ubicacion, franquicia);
				Teatros.agregarVertice(nombre, nuevo);
			}
		}catch (Exception e) {
			System.out.println(e.toString());
		}
		return false;
	}
	
	
	@Override
	public boolean cargarCartelera(String ruta) {
		try{
			for (int i = 1; i < 6; i++) 
			{
				String nuevaruta= "Data/programacion/dia"+i+".json";
				//System.out.println("Leyendo arcchivo: "+ nuevaruta);

				FileReader fl = new FileReader(nuevaruta);
				JsonParser parser= new JsonParser();
				JsonArray arreglo = (JsonArray) parser.parse(fl);
				Iterator<JsonElement> iter = arreglo.iterator();
				
				while(iter.hasNext())
				{
					JsonObject actual = (JsonObject) iter.next();
					//Busca el teatro
					String buscar = actual.get("teatros").getAsString();
					VOTeatro lteatro= Teatros.darVertice(buscar).getValor();
					JsonObject teatro=(JsonObject) actual.get("teatro");
					//arreglo de id peliculas
					JsonArray arreglopeliculas = (JsonArray) teatro.getAsJsonArray("peliculas");
					Iterator<JsonElement> iterpelis = arreglopeliculas.iterator();
					//System.out.println("Teatro: "+lteatro.getNombre());
					//Recorre las peliculas
					while(iterpelis.hasNext())
					{
						JsonObject peliactual = (JsonObject) iterpelis.next();
						Integer id = peliactual.get("id").getAsInt();
						//Busca la peli con  el id en el arbol
						VOPelicula lpelicula= peliculas.get(id);
						//Arreglo de funciones de la pelicula
						JsonArray funciones = (JsonArray) peliactual.getAsJsonArray("funciones");
						Iterator<JsonElement> iterfunc = funciones.iterator();
						RedBlackBST<Integer,VOHorario> horarios=  lteatro.getHorarios();
						//System.out.println("Pelicula: "+lpelicula.getTitulo());
						while(iterfunc.hasNext()){
							//llena el arbol de horarios
							JsonObject funcion = (JsonObject) iterfunc.next();
							String hora = funcion.get("hora").getAsString();
							if(hora.contains("a")){
								hora = funcion.get("hora").getAsString().substring(0, 4)+" am";
							}else{
								hora = funcion.get("hora").getAsString().substring(0, 4)+" pm";
							}
							DateFormat fta= new SimpleDateFormat("hh:mm a");
							Date fecha =  (Date) fta.parse(hora);
							//System.out.println( "Hora:"+fecha.getHours());
							VOHorario horario;
							if(horarios.contains(fecha.getHours())){
								horario = horarios.get(fecha.getHours());
								horario.agregarPelicula(id, lpelicula);
							}else{
								horario = new VOHorario(); 
								horario.setHora(fecha);
								horario.agregarPelicula(id, lpelicula);
								horarios.put(fecha.getHours(),horario);
							}
							//System.out.println(horarios.size());
						}
						//agrega los horarios al teatro
						lteatro.setHorarios(horarios);
					}
				}
				grafos.put(i,Teatros);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	
	public boolean cargarSimilitudes(){
		try{
			FileReader fl = new FileReader("Data/simMatriz.json");
			JsonParser parser= new JsonParser();
			JsonArray arreglo = (JsonArray) parser.parse(fl);
			Iterator<JsonElement> iter = arreglo.iterator();
			while(iter.hasNext()){
				JsonObject actual = (JsonObject) iter.next();
				Iterator<VOPelicula> iterpelis = peliculas.darLista().iterator();
				Integer idpelicula = actual.get("movieId").getAsInt();
				RedBlackBST<Integer,Double> simi= new RedBlackBST<>();
				while (iterpelis.hasNext()) {
					VOPelicula lPelicula = (VOPelicula) iterpelis.next();
					Integer idactual = lPelicula.getId();
					String lt=actual.get(idactual.toString()).getAsString();
					double similitud ;
					if(lt.equals("NaN")){
						similitud=0;
					}else{
						similitud=Double.parseDouble(lt);
					}
					simi.put(idactual,similitud);
				}
				similitudes.put(idpelicula, simi);
			}
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public void cargarpeliculas(){
		
		try{
			FileReader fl = new FileReader("Data/movies_filtered.json");
			JsonParser parser= new JsonParser();
			JsonArray arreglo = (JsonArray) parser.parse(fl);
			Iterator<JsonElement> iter = arreglo.iterator();
			while(iter.hasNext()){
				JsonObject actual = (JsonObject) iter.next();
				Integer id = actual.get("movie_id").getAsInt();
				String titulo=actual.get("title").getAsString();
				String[] generos = actual.get("genres").getAsString().split("\\|");
				VOPelicula nueva = new VOPelicula(id, titulo, generos);
				peliculas.put(id,nueva);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}	
	}
	
	
	@Override
	public boolean cargarRed(String ruta) {
		try{
			FileReader fl = new FileReader(ruta);
			JsonParser parser= new JsonParser();
			JsonArray arreglo = (JsonArray) parser.parse(fl);
			Iterator<JsonElement> iter = arreglo.iterator();
			while(iter.hasNext()){
				JsonObject actual = (JsonObject) iter.next();
				String salida = actual.get("Teatro 1").getAsString();
				String llegada = actual.get("Teatro 2").getAsString();
				double tiempo = actual.get("Tiempo (minutos)").getAsDouble();
				Teatros.agregarArco(salida, llegada, tiempo);
				Teatros.agregarArco(llegada, salida, tiempo);
				
			}
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public void cargarUsuarios(){
		try {
			BufferedReader br = new BufferedReader(new FileReader("Data/ratings_filtered.csv"));
			br.readLine();
			String line = br.readLine();
			int idUsuario;
			int idPelicula;
			int rating;
			while (line!=null) {
				String x[]=line.split(",");
				idUsuario=Integer.parseInt(x[0]);
				idPelicula=Integer.parseInt(x[1]);
				rating=Integer.parseInt(x[2]);
				VOUsuario nuevo ;
				if(!usuarios.contains(idUsuario)){
					nuevo = new VOUsuario();
					nuevo.setId(idUsuario);
					nuevo.setRatings(idPelicula, rating);
					usuarios.put(idUsuario, nuevo);
				}else{
					nuevo=usuarios.get(idUsuario);
					nuevo.setRatings(idPelicula, rating);
				}
				line=br.readLine();
			}			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public int sizeMovies() {
		return peliculas.size();
	}
	@Override
	public int sizeTeatros() {
		return Teatros.numVertices();
	}
	
	//Requerimiento 4
	@Override
	public ListaEncadenada<VOPeliculaPlan> PlanPeliculas(VOUsuario usuario, int fecha) {
		
		WeightedDirectedGraph<String, VOTeatro> lgrafo = grafos.get(fecha);
		ListaEncadenada<VOPeliculaPlan>  rta = new ListaEncadenada<>();
		RedBlackBST<Integer, VOPeliculaPlan> planHorarios= new RedBlackBST<>();
		RedBlackBST<Integer, VOPeliculaPlan> arbolrespuesta = new RedBlackBST<>();
		VOUsuario lusuario = usuarios.get(usuario.getId());

		int horas=HORASFESTIVAL;

		VOPelicula ter= null;

		ColaPrioridad<VOTeatro> teatrosPorHorarios = new ColaPrioridad<>();
		teatrosPorHorarios.crearCP(46);
		Iterator<Vertice<String, VOTeatro>> iter = lgrafo.darVertices();
		while (iter.hasNext()) {
			Vertice<String,VOTeatro> vertice = iter.next();
			VOTeatro teatro = vertice.getValor();
			try{
				teatrosPorHorarios.agregar(teatro);
			}catch(Exception e){
				System.out.println("Error agregar a la cola");
			}
		}

		Integer ratingBase = lusuario.getRatings().max();
		
		ListaEncadenada<VOTeatro> cola = teatrosPorHorarios.darCola();
		for (int i = 0; i < cola.darNumeroElementos()&&horas >0; i++) {
			VOTeatro teatro = cola.darElemento(i);
			//System.out.println("Teatro : "+teatro.getNombre()+"///"+teatro.getHorarios().size());
			for (int j = 0; j < teatro.getHorarios().size(); j++) {
				VOHorario horarioA = teatro.getHorarios().darLista().darElemento(j);
				//System.out.println("Hora: "+horarioA.getHora().getHours());
				ListaEncadenada<VOPelicula> lpeliculas = horarioA.getPelicula().darLista();
				int a = 0;
				for (int k = 0; k < lpeliculas.darNumeroElementos(); k++) {
					VOPelicula option = lpeliculas.darElemento(k);
					Double simi=similitudes.get(ratingBase).get(option.getId())*1000;
					if(simi.intValue()!=1000 && !arbolrespuesta.contains(option.getId())){
						if(a < simi.intValue()){
							a= simi.intValue();
							ter=option;
						}
					}
					//System.out.println("pelis :"+option.getTitulo()+"Simi: "+ simi );
				}
				
				VOPeliculaPlan nueva = new VOPeliculaPlan();
				nueva.setDia(fecha);
				nueva.setPelicula(ter);
				nueva.setTeatro(teatro);
				nueva.setHoraInicio(horarioA.getHora());
				arbolrespuesta.put(ter.getId(), nueva);
				horas=horas-2;
				//System.out.println("\n \n --------- PELICULA INSERTADA   : "+ter.getTitulo()+" --------------\n \n");
			}			
			//System.out.println("----------------CAMBIO TEATRO---------------------------");
		}
		
		 Iterator<Integer> iterres = arbolrespuesta.keys().iterator();
		while (iterres.hasNext()) {
			VOPeliculaPlan  vo= arbolrespuesta.get(iterres.next());
			int hh=vo.getHoraInicio().getHours();
			planHorarios.put(hh, vo);
		}
		
		for (int i = 8; i < 23;i=i+2 ) {
			rta.agregarElementoFinal(planHorarios.get(i));
		}
		return rta;
	}
	@Override
	//Requerimiento 5
	public ILista<VOPeliculaPlan> PlanPorGenero(VOGeneroPelicula genero, VOUsuario usuario) {


		ListaEncadenada<ListaEncadenada<VOPeliculaPlan>>  listarta = new ListaEncadenada<>();
		
		VOTeatro anterior=null;
		
		int horaActual=8;
		int horadia =800;
		
		ListaEncadenada<VOPeliculaPlan>  rta = new ListaEncadenada<>();
		
		RedBlackBST<String, VOTeatro> teatrosVisitados=new RedBlackBST<>();

		for(int dia=1 ; dia<6;dia++){

			WeightedDirectedGraph<String, VOTeatro> lgrafo = grafos.get(dia);
			RedBlackBST<Integer, VOPeliculaPlan> planHorarios= new RedBlackBST<>();
			RedBlackBST<Integer, VOPeliculaPlan> arbolrespuesta = new RedBlackBST<>();
			VOUsuario lusuario = usuarios.get(usuario.getId());

			Integer horas=HORASFESTIVAL;
			
			Double tempo = 0.0;

			Double Inicial=Math.random()*46;
			VOTeatro teatroInicial=null;
			
			Iterator<Vertice<String, VOTeatro>> itertea = grafos.get(1).darVertices();
			for (int i=0;itertea.hasNext();i++) {
				Vertice<String,VOTeatro> vertice = itertea.next();
				if(i==Inicial.intValue()){
					teatroInicial = vertice.getValor();
				}
			}
			
			ColaPrioridad<VOTeatro> teatrosPorDistancias = new ColaPrioridad<>();
			teatrosPorDistancias.crearCP(46);
			Iterator<Vertice<String, VOTeatro>> iter = lgrafo.darVertices();
			while (iter.hasNext()) {
				Vertice<String,VOTeatro> vertice = iter.next();
				VOTeatro teatro = vertice.getValor();
				try{
					teatrosPorDistancias.agregar(teatro);
				}catch(Exception e){
					System.out.println("Error agregar a la cola");
				}
			}

			Integer ratingBase = lusuario.getRatings().max();

			ListaEncadenada<VOTeatro> cola = teatrosPorDistancias.darCola();
			VOTeatro teatro = null;
			for (int i = 0; i < cola.darNumeroElementos() && horaActual<=22; i++) {
				
				if(anterior==null){
					teatro = teatroInicial;
				}else{
					teatro= darTeatroCercano(anterior,teatrosVisitados);
				}
				if(anterior!=null){
					double peso = lgrafo.darPesoArco(anterior.getNombre(), teatro.getNombre());
					//12-6 15 %
					if(horadia>=1200 && horadia <= 1800){
						tempo=horas-((peso*1.15)/100);
						horas = tempo.intValue();
						horaActual=horaActual+2;
					}else
					//6-12pm 20%	
					if(horadia>1800 && horadia<=2200){
						tempo=horas-((peso*1.2)/100);
						horas = tempo.intValue();
						horaActual=horaActual+2;
					}else{
						tempo=horas-(peso/100);
						horas = tempo.intValue();
						horaActual=horaActual+2;
					}
				}
				//System.out.println("Teatro : "+teatro.getNombre()+"///"+teatro.getHorarios().size());
				for (int j = 0; j < teatro.getHorarios().size()&&horaActual<=22; j++) {
					//System.out.println("Hora Actual:"+horaActual);
					VOHorario horarioA = teatro.getHorarios().get(horaActual);
					//System.out.println("Hora: "+horarioA.getHora().getHours());
					ListaEncadenada<VOPelicula> lpeliculas = horarioA.getPelicula().darLista();
					int a = 0;
					VOPelicula ter= null;
					for (int k = 0; k < lpeliculas.darNumeroElementos(); k++) {
						VOPelicula option = lpeliculas.darElemento(k);
						if(option.esGenero(genero.getNombre())){
							Double simi=similitudes.get(ratingBase).get(option.getId())*1000;
							if(simi.intValue()!=1000 && !arbolrespuesta.contains(option.getId())){
								if(a < simi.intValue()){
								a= simi.intValue();
								ter=option;
								}
							}
						}
						//System.out.println("pelis :"+option.getTitulo()+"Simi: "+ simi );
					}
					if(ter!=null){
					VOPeliculaPlan nueva = new VOPeliculaPlan();
					nueva.setDia(dia);
					nueva.setPelicula(ter);
					nueva.setTeatro(teatro);
					nueva.setHoraInicio(horarioA.getHora());
					arbolrespuesta.put(ter.getId(), nueva);
					//System.out.println("Pelicula: "+ter.getTitulo()+" Hora: "+horarioA.getHora().getHours()+" Generos :"+ter.toGeneros());
					horas=horas-2;
					horaActual=horaActual+2;
					}
					//System.out.println("\n \n --------- PELICULA INSERTADA   : "+ter.getTitulo()+" --------------\n \n");
				}
				teatrosVisitados.put(teatro.getNombre(), teatro);
				anterior=teatro;
				//System.out.println("----------------CAMBIO TEATRO---------------------------");
			}

			Iterator<Integer> iterres = arbolrespuesta.keys().iterator();
			while (iterres.hasNext()) {
				VOPeliculaPlan  vo= arbolrespuesta.get(iterres.next());
				int hh=vo.getHoraInicio().getHours();
				planHorarios.put(hh, vo);
			}

			for (int i = 8; i < 23;i=i+2 ) 
			{
				if(planHorarios.contains(i)){
				rta.agregarElementoFinal(planHorarios.get(i));
				}
			}
			listarta.agregarElementoFinal(rta);
		}
		
		for (int i = 0; i < 5; i++) {
			//System.out.println("cantidad: "+listarta.darElemento(i).darNumeroElementos());
			if(listarta.darElemento(i).darNumeroElementos() > rta.darNumeroElementos() ){
				rta=listarta.darElemento(i);
			}
		}
		return rta;
	}
	
	private VOTeatro darTeatroCercano(VOTeatro inicial , RedBlackBST<String, VOTeatro> visitados ){

		WeightedDirectedGraph<String, VOTeatro> lgrafo = grafos.get(1);
		Iterator<Vertice<String, VOTeatro>> iter = lgrafo.darVertices();
		VOTeatro rta= null;
		double lpeso=10000;
		while (iter.hasNext()) {
			Vertice<String,VOTeatro> vertice = iter.next();
			if(!vertice.getValor().getNombre().equals(inicial.getNombre())&&!visitados.contains(vertice.getLlave())){
				if(lgrafo.darPesoArco(inicial.getNombre(), vertice.getLlave())<lpeso){
					rta=vertice.getValor();
					lpeso=lgrafo.darPesoArco(inicial.getNombre(), vertice.getLlave());
				}
			}
		}
		//System.out.println("Salida: "+inicial.getNombre()+" llegada:"+rta.getNombre()+" peso: "+lpeso);
		return rta;
	}
	
	private VOTeatro darTeatroCercanoFranquicia(VOTeatro inicial , RedBlackBST<String, VOTeatro> visitados , String franquicia ){

		WeightedDirectedGraph<String, VOTeatro> lgrafo = grafos.get(1);
		Iterator<Vertice<String, VOTeatro>> iter = lgrafo.darVertices();
		VOTeatro rta= null;
		double lpeso=10000;
		while (iter.hasNext()) {
			Vertice<String,VOTeatro> vertice = iter.next();
			if((vertice.getValor().getFranquicia().getNombre()).equals(franquicia)){
			if(!vertice.getValor().getNombre().equals(inicial.getNombre())&&!visitados.contains(vertice.getLlave())){
				if(lgrafo.darPesoArco(inicial.getNombre(), vertice.getLlave())<lpeso){
					rta=vertice.getValor();
					lpeso=lgrafo.darPesoArco(inicial.getNombre(), vertice.getLlave());
				}
			}
			}
		}
		//System.out.println("Salida: "+inicial.getNombre()+" llegada:"+rta.getNombre()+" peso: "+lpeso);
		return rta;
	}
	
	@Override
	public ILista<VOPeliculaPlan> PlanPorFranquicia(VOFranquicia franquicia, int fecha, String franja) {
		int horaActual;
		int horasDisponibles;
		int horaLimite;
		int horadia;
		if(franja.equals("MA�ANA")){
			horaActual=8;
			horasDisponibles=5;
			horaLimite=12;
		}else if(franja.equals("TARDE"))
		{
			horaActual=12;
			horasDisponibles=6;
			horaLimite=18;
		}else if(franja.equals("NOCHE")){
			horaActual=18;
			horasDisponibles=5;
			horaLimite=22;
		}else{
			horaActual=8;
			horasDisponibles=14;
			horaLimite=22;
		}
		
		WeightedDirectedGraph<String, VOTeatro> lgrafo = grafos.get(fecha);
		ColaPrioridad<VOTeatro> teatrosPorDistancias = new ColaPrioridad<>();
		teatrosPorDistancias.crearCP(46);
		Iterator<Vertice<String, VOTeatro>> iter = lgrafo.darVertices();
		while (iter.hasNext()) {
			Vertice<String,VOTeatro> vertice = iter.next();
			VOTeatro teatro = vertice.getValor();
			try{
				teatrosPorDistancias.agregar(teatro);
			}catch(Exception e){
				System.out.println("Error agregar a la cola");
			}
		}
		
		horadia=horaActual*100;
		VOTeatro teatroInicial=null;
		Iterator<Vertice<String, VOTeatro>> itertea = lgrafo.darVertices();
		RedBlackBST<Integer, VOPeliculaPlan >arbolrespuesta= new RedBlackBST<>();
		boolean encontro= false;
		
		while (itertea.hasNext()&& !encontro) {
			Vertice<String,VOTeatro> vertice = itertea.next();
			if(franquicia.getNombre().equalsIgnoreCase(vertice.getValor().getFranquicia().getNombre())){
				teatroInicial = vertice.getValor();
				encontro=true;
			}
		}
		
		ListaEncadenada<VOPeliculaPlan>  rta = new ListaEncadenada<>();
		RedBlackBST<String, VOTeatro> teatrosVisitados=new RedBlackBST<>();
		
		VOTeatro anterior=null;
		VOTeatro teatro=null;
		Double tempo;
		ListaEncadenada<VOTeatro> cola = teatrosPorDistancias.darCola();
		
		for (int i = 0; i <cola.darNumeroElementos()&& horaActual<=22&&horaActual<=horaLimite; i++) {
			if(franquicia.getNombre().equals(cola.darElemento(i).getFranquicia().getNombre())){
				if(anterior==null){
					teatro = teatroInicial;
				}else{
					teatro= cola.darElemento(i);
				}
				if(anterior!=null){
					double peso = lgrafo.darPesoArco(anterior.getNombre(), teatro.getNombre());
					//12-6 15 %
					if(horadia>=1200 && horadia <= 1800){
						tempo=horasDisponibles-((peso*1.15)/100);
						horasDisponibles = tempo.intValue();
						horaActual=horaActual+2;
					}else
						//6-12pm 20%	
						if(horadia>1800 && horadia<=2200){
							tempo=horasDisponibles-((peso*1.2)/100);
							horasDisponibles = tempo.intValue();
							horaActual=horaActual+2;
						}else{
							tempo=horasDisponibles-(peso/100);
							horasDisponibles = tempo.intValue();
							horaActual=horaActual+2;
						}
				}
				//System.out.println("Teatro : "+teatro.getNombre()+"///"+teatro.getHorarios().size());
				for (int j = 0; j < teatro.getHorarios().size()&&horaActual<=22&&horaActual<=horaLimite; j++) {
					//System.out.println("Hora Actual:"+horaActual);
					VOHorario horarioA = teatro.getHorarios().get(horaActual);
					//System.out.println("Hora: "+horarioA.getHora().getHours());
					ListaEncadenada<VOPelicula> lpeliculas = horarioA.getPelicula().darLista();
					int a = 0;
					VOPelicula ter= lpeliculas.darElemento(j);
					if(ter!=null){
						VOPeliculaPlan nueva = new VOPeliculaPlan();
						nueva.setDia(fecha);
						nueva.setPelicula(ter);
						nueva.setTeatro(teatro);
						nueva.setHoraInicio(horarioA.getHora());
						arbolrespuesta.put(ter.getId(), nueva);
						//System.out.println("Pelicula: "+ter.getTitulo()+" Hora: "+horarioA.getHora().getHours()+" teatro :"+teatro.getNombre());
						horasDisponibles=horasDisponibles-2;
						horaActual=horaActual+2;
					}
					//System.out.println("\n \n --------- PELICULA INSERTADA   : "+ter.getTitulo()+" --------------\n \n");
				}
				teatrosVisitados.put(teatro.getNombre(), teatro);
				anterior=teatro;

			}//System.out.println("----------------CAMBIO TEATRO---------------------------");
		}

		Iterator<Integer> iterres = arbolrespuesta.keys().iterator();
		RedBlackBST<Integer, VOPeliculaPlan> planHorarios= new RedBlackBST<>();
		while (iterres.hasNext()) {
			VOPeliculaPlan  vo = arbolrespuesta.get(iterres.next());
			int hh=vo.getHoraInicio().getHours();
			planHorarios.put(hh, vo);
		}

		for (int i = 8; i < 23;i=i+2 ) 
		{
			if(planHorarios.contains(i)){
			rta.agregarElementoFinal(planHorarios.get(i));
			}
		}
		
		return rta ;
	}
	@Override
	//rq7
public ILista<VOPeliculaPlan> PlanPorGeneroYDesplazamiento(VOGeneroPelicula genero, int fecha) {
		
		ListaEncadenada<VOPelicula> peliculasGenero= new ListaEncadenada<>();
		WeightedDirectedGraph<String, VOTeatro> lgrafo = grafos.get(fecha);
		ListaEncadenada<VOPeliculaPlan> rta = new ListaEncadenada<>();
		int horaActual=8;
		Iterator<VOPelicula> iter = peliculas.darLista().iterator();
		while (iter.hasNext()) {
			VOPelicula lpelicula =  iter.next();
			if(lpelicula.esGenero(genero.getNombre())){
				//System.out.println("Titulo: "+lpelicula.getTitulo()+" Genero: "+lpelicula.toGeneros());
				peliculasGenero.agregarElementoFinal(lpelicula);
			}
		}
		
		ColaPrioridad<VOTeatro> teatrosPorDistancias = new ColaPrioridad<>();
		teatrosPorDistancias.crearCP(46);
		Iterator<Vertice<String, VOTeatro>> itergrafo = lgrafo.darVertices();
		while (itergrafo.hasNext()) {
			Vertice<String,VOTeatro> vertice = itergrafo.next();
			VOTeatro teatro = vertice.getValor();
			try{
				teatrosPorDistancias.agregar(teatro);
				//System.out.println("Teatro: "+teatro.getNombre()+" cad: "+teatro.getHorarios().size());
			}catch(Exception e){
				System.out.println("Error agregar a la cola");
			}
		}
		
		Double Inicial=Math.random()*46;
		VOTeatro teatroInicial=null;
		
		Iterator<Vertice<String, VOTeatro>> itertea = grafos.get(1).darVertices();
		for (int i=0;itertea.hasNext();i++) {
			Vertice<String,VOTeatro> vertice = itertea.next();
			if(i==Inicial.intValue()){
				teatroInicial = vertice.getValor();
			}
		}
		VOTeatro teatro=null;
		VOTeatro anterior=null;
		VOPelicula option=null;
		RedBlackBST<String, VOTeatro> teatrosVisitados=new RedBlackBST<>();
		RedBlackBST<Integer, VOPelicula> peliculasVistas=new RedBlackBST<>();
		RedBlackBST<Integer, VOPeliculaPlan >arbolrespuesta=new RedBlackBST<>();
		ListaEncadenada<VOTeatro> cola = teatrosPorDistancias.darCola();
		for (int i = 0; i < cola.darNumeroElementos()&& horaActual<=22 ; i++) {
			if(anterior==null){
				teatro = teatroInicial;
			}else{
				teatro= darTeatroCercano(anterior,teatrosVisitados);
			}
			for (int j = 0; j < teatro.getHorarios().size()&&horaActual<=22; j++) {
				//System.out.println(horaActual);
				VOHorario horarioA = teatro.getHorarios().get(horaActual);
				for (int k = 0; k < peliculasGenero.darNumeroElementos() ; k++) {
					option = peliculasGenero.darElemento(k);
					if(horarioA.estaPeli(option)&&!peliculasVistas.contains(option.getId())){
						peliculasVistas.put(option.getId(), option);
						horaActual=horaActual+2;
						VOPeliculaPlan nueva = new VOPeliculaPlan();
						nueva.setDia(fecha);
						nueva.setPelicula(option);
						nueva.setTeatro(teatro);
						nueva.setHoraInicio(horarioA.getHora());
						arbolrespuesta.put(option.getId(), nueva);
						break;
					}
				}
			}
			anterior=teatro;
			horaActual+=2;
			teatrosVisitados.put(anterior.getNombre(), anterior);
		}
		RedBlackBST<Integer, VOPeliculaPlan> planHorarios= new RedBlackBST<>();
		Iterator<Integer> iterres = arbolrespuesta.keys().iterator();
		while (iterres.hasNext()) {
			VOPeliculaPlan  vo= arbolrespuesta.get(iterres.next());
			int hh=vo.getHoraInicio().getHours();
			planHorarios.put(hh, vo);
		}

		for (int i = 8; i < 23;i=i+2 ) 
		{
			if(planHorarios.contains(i)){
			//System.out.println("Pelicula: "+planHorarios.get(i).getPelicula().getTitulo()+" hora: "+planHorarios.get(i).getHoraInicio().getHours()+" teatro: "+planHorarios.get(i).getTeatro().getNombre());
			rta.agregarElementoFinal(planHorarios.get(i));
			}
		}
		return rta;
	}
	@Override
	public ILista<VOPeliculaPlan> PlanPorGeneroDesplazamientoYFranquicia(VOGeneroPelicula genero, int fecha,VOFranquicia franquicia) {
		
		ListaEncadenada<VOPeliculaPlan>respuesta=new ListaEncadenada<>();
		ListaEncadenada<VOPelicula> peliculasGenero= new ListaEncadenada<>();
		RedBlackBST<Integer, VOPelicula> peliculasVistas=new RedBlackBST<>();
		
		
		Iterator<VOPelicula> iter = peliculas.darLista().iterator();
		while (iter.hasNext()) {
			VOPelicula lpelicula =  iter.next();
			if(lpelicula.esGenero(genero.getNombre())){
				//System.out.println("Titulo: "+lpelicula.getTitulo()+" Genero: "+lpelicula.toGeneros());
				peliculasGenero.agregarElementoFinal(lpelicula);
			}
		}
		
			int horaActual=8;
			WeightedDirectedGraph<String, VOTeatro> lgrafo = grafos.get(fecha);
			ColaPrioridad<VOTeatro> teatrosFranquiciasPorHorarios = new ColaPrioridad<>();
			teatrosFranquiciasPorHorarios.crearCP(46);
			Iterator<Vertice<String, VOTeatro>> itergrafo = lgrafo.darVertices();
			while (itergrafo.hasNext()) {
				Vertice<String,VOTeatro> vertice = itergrafo.next();
				VOTeatro teatro = vertice.getValor();
				if(teatro.getFranquicia().getNombre().equalsIgnoreCase(franquicia.getNombre())){
					try{
						teatrosFranquiciasPorHorarios.agregar(teatro);
						//System.out.println("Teatro: "+teatro.getNombre()+" cad: "+teatro.getHorarios().size());
					}catch(Exception e){
						System.out.println("Error agregar a la cola");
					}
				}
			}

			VOTeatro teatroInicial=null;

			Iterator<Vertice<String, VOTeatro>> itertea = grafos.get(1).darVertices();
			while(itertea.hasNext()) {
				Vertice<String,VOTeatro> vertice = itertea.next();
				if((vertice.getValor().getFranquicia().getNombre()).equalsIgnoreCase(franquicia.getNombre())){
					teatroInicial = vertice.getValor();
					break;
				}
			}
			VOTeatro teatro=null;
			VOTeatro anterior=null;
			VOPelicula option=null;
			boolean vio = false;
			RedBlackBST<String, VOTeatro> teatrosVisitados=new RedBlackBST<>();

			ListaEncadenada<VOTeatro> cola = teatrosFranquiciasPorHorarios.darCola();
			for (int i = 0; i < cola.darNumeroElementos()&& horaActual<=22 ; i++) {
				vio=false;
				if(anterior==null){
					teatro = teatroInicial;
				}else{
					if(vio){
					teatro= darTeatroCercanoFranquicia(teatro, teatrosVisitados, franquicia.getNombre());
					}else{
					teatro= darTeatroCercanoFranquicia(anterior, teatrosVisitados, franquicia.getNombre());
					}
				}
				for (int j = 0; j < teatro.getHorarios().size()&&horaActual<=22; j++) {
	
					VOHorario horarioA = teatro.getHorarios().get(horaActual);
					for (int k = 0; k < peliculasGenero.darNumeroElementos() ; k++) {
						option = peliculasGenero.darElemento(k);
						if(horarioA.estaPeli(option)&&!peliculasVistas.contains(option.getId())){
							peliculasVistas.put(option.getId(), option);
							horaActual=horaActual+2;
							vio=true;
							VOPeliculaPlan nueva = new VOPeliculaPlan();
							nueva.setDia(fecha);
							nueva.setPelicula(option);
							nueva.setTeatro(teatro);
							nueva.setHoraInicio(horarioA.getHora());
							System.out.println("Pelicula:"+option.getTitulo()+
									"\n Hora: "+horarioA.getHora().getHours()+
									"\n Dia:"+fecha+
									"\n Teatro:  "+teatro.getNombre());
							respuesta.agregarElementoFinal(nueva);
							break;
						}
					}
				}
				anterior=teatro;
				if(vio){
					
					horaActual+=2;
				}
				teatrosVisitados.put(anterior.getNombre(), anterior);
			}
		return respuesta ;
	}
	@Override
	public ILista<Vertice<String, VOTeatro>> generarMapa() {
		
		ListaEncadenada<Vertice<String, VOTeatro>> rta = new ListaEncadenada<>();
		
		Dijkstra dj =new Dijkstra(Teatros, 1);
		Stack<Arco> p = (Stack<Arco>) dj.pathTo(43);
		Iterator<Arco> it = p.iterator();
		
		while(it.hasNext())
		{
			Arco a = it.next();
			Vertice<String, VOTeatro> ll = a.getLlegada();
			Vertice<String, VOTeatro> sa = a.getSalida();
			
			if(ll != null)
			{
				rta.agregarElementoFinal(ll);
			}
			if(sa != null)
			{
				rta.agregarElementoFinal(sa);
			}
		}
		
		return rta;
	}
	@Override
	public ListaEncadenada<ListaEncadenada<VOTeatro>> rutasPosible(VOTeatro origen, int n) {
		int cuenta=0;
		ListaEncadenada< ListaEncadenada< VOTeatro > >listarutas= new ListaEncadenada<>();
		Iterator<Vertice<String, VOTeatro>> iter = Teatros.darVertices();
		while (iter.hasNext()) {
			Vertice<String,VOTeatro> vertice = (Vertice<String,VOTeatro>) iter.next();
			NodoCamino<String> rta = Teatros.DFS(origen.getNombre());
			ListaEncadenada<String> lista = rta.getCamino();
			ListaEncadenada<VOTeatro> nueva = new ListaEncadenada<>();
			cuenta =0;
			for (int i = 0; i < lista.darNumeroElementos()&& cuenta < n; i++) {
				VOTeatro lteatro= new VOTeatro(lista.darElemento(i), null , null );
				nueva.agregarElementoFinal(lteatro);
				cuenta++;
			}
			listarutas.agregarElementoFinal(nueva);
		}
		return listarutas;
	}
	
	
}
